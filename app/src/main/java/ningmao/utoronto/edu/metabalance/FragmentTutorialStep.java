package ningmao.utoronto.edu.metabalance;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ning on 2016-01-24.
 */
public class FragmentTutorialStep extends Fragment {

    @SuppressWarnings("unused")
    private static final String TAG = FragmentTutorialStep.class.getSimpleName();
    public static final String ARG_SECTION_NUMBER = "section_number";

    private ImageView mImagePlaceholder;

    // instantiate FragmentTutorialStep with the selected step fragment in the viewpager
    public static FragmentTutorialStep newInstance(int position) {
        FragmentTutorialStep myFragment = new FragmentTutorialStep();

        Bundle args = new Bundle();
        args.putInt(FragmentTutorialStep.ARG_SECTION_NUMBER, position);

        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tutorial_step,container, false);

        // link the layout objects to corresponding UI elements
        mImagePlaceholder = (ImageView) rootView.findViewById(R.id.fragment_tutorial_imageview_step);

        int section_number = getArguments().getInt(ARG_SECTION_NUMBER);


        switch (section_number) {
            case 0: mImagePlaceholder.setImageResource(R.drawable.tutorial_step_1); break;
            case 1: mImagePlaceholder.setImageResource(R.drawable.tutorial_step_2); break;
            case 2:	mImagePlaceholder.setImageResource(R.drawable.tutorial_step_3); break;
            case 3:	mImagePlaceholder.setImageResource(R.drawable.tutorial_step_4); break;
            case 4: mImagePlaceholder.setImageResource(R.drawable.tutorial_step_5); break;
            case 5: mImagePlaceholder.setImageResource(R.drawable.tutorial_step_6); break;
            case 6:	mImagePlaceholder.setImageResource(R.drawable.tutorial_step_7); break;
        }

        return rootView;
    }
}
