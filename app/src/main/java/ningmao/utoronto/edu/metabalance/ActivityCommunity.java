package ningmao.utoronto.edu.metabalance;

import android.app.ListActivity;
import android.os.Bundle;

import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;


/**
 * Created by ning on 2016-01-23.
 */
public class ActivityCommunity extends ListActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        final SearchTimeline searchTimeline = new SearchTimeline.Builder()
                .query("#metabalance")
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                .setTimeline(searchTimeline)
                .build();
        setListAdapter(adapter);
    }



}
