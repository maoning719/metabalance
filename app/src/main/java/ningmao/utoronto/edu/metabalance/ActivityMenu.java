package ningmao.utoronto.edu.metabalance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import Helpers.DatabaseHelper;
import Helpers.FragmentHelper;
/**
 * Created by ning on 2016-01-23.
 */
public class ActivityMenu extends Activity implements View.OnClickListener {
    private static final String TAG = ActivityMenu.class.getSimpleName();
    private DatabaseHelper mDatabaseHelper = null;

    private Button mButtonExercises;
    private Button mButtonProgress;
    private Button mButtonTutorial;
    private Button mButtonCommunity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);

        // Create database with static data for now
        mDatabaseHelper = new DatabaseHelper(this);

        // Link layout elements to code
        mButtonExercises = (Button) findViewById(R.id.activity_main_button_exercises);
        mButtonProgress = (Button) findViewById(R.id.activity_main_button_progress);
        mButtonTutorial = (Button) findViewById(R.id.activity_main_button_tutorial);
        mButtonCommunity = (Button) findViewById(R.id.activity_main_button_community);


        // register listeners
        mButtonExercises.setOnClickListener(this);
        mButtonProgress.setOnClickListener(this);
        mButtonTutorial.setOnClickListener(this);
        mButtonCommunity.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId()) {
            case R.id.activity_main_button_exercises:
                // first get all the prescribed exercises from remote server
                intent = new Intent(this, ActivityMeasure.class);
                startActivity(intent);
                break;
            case R.id.activity_main_button_progress:
                intent = new Intent(this, ActivityProgress.class);
                startActivity(intent);
                break;
            case R.id.activity_main_button_tutorial:
                intent = new Intent(this, ActivityTutorial.class);
                startActivity(intent);
                break;
            case R.id.activity_main_button_community:
                intent = new Intent(this, ActivityCommunity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
