package ningmao.utoronto.edu.metabalance;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import android.os.Handler;
import android.widget.Toast;


import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.URL;

import Accelerometers.Accelerometer;
import Accelerometers.AccelerometerManager;
import Helpers.DatabaseHelper;
import Helpers.PrefUtils;
import Helpers.SoundPoolHelper;
import io.fabric.sdk.android.Fabric;

/**
 * Created by ning on 2016-01-22.
 */
public class ActivityMeasure extends Activity implements Accelerometer.AccelerometerListener {

    public static final String TAG = ActivityMeasure.class.getSimpleName();

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private float absement = 0;
    private double roundedAbsement;
    private CountDownTimer timer;
    long millisInFuture = 30000; //30 seconds
    long countDownInterval = 1000; //1 second

    private static final int STATE_INIT = 1;
    private static final int STATE_COUNTDOWN = 2;
    private static final int STATE_MEASURE = 3;
    private static final int STATE_FINISH = 4;
    private MyCounter mTimer;

    // SoundPool
    private SoundPoolHelper mSoundPoolHelper;

    // Starting measurement state
    private volatile int mState = STATE_INIT;

    // Hardware
    private Vibrator mVibrator;
    private Accelerometer mAccelerometer;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Thread mMeasureThread = null;

    // Views
    private TextView mTextTitle, mTextTime, mTextSubtitle, mTextResult;
    private ImageView mImageComposeTweet;
    private String msg;

    TextView balanceTextView;
    long startTime = 0;

    private boolean isMeasuring = false;
    private boolean isTweeting = false;

    // progress bar
    CircleProgressBar CircleProgressBar;

    // database
    private DatabaseHelper mDatabaseHelper = null;

    private String ConsumerKey = "caQQb2F7rBnvUoS09bMMmnnZK";
    private String ConsumerSecret = "Tj8g0Ae7J5PI607jE5JV61SZabgoeTWz42kCX5FRjFCm9DlIk9";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timed_measure);

        TwitterAuthConfig authConfig =  new TwitterAuthConfig(ConsumerKey, ConsumerSecret);
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());

        balanceTextView = (TextView) findViewById(R.id.balance);

        mDatabaseHelper = new DatabaseHelper(this);

        // Init views
        mTextTitle = (TextView) findViewById(R.id.fragment_exercise_measure_textview_title);
        mTextTime = (TextView) findViewById(R.id.fragment_exercise_measure_textview_time);
        mTextSubtitle = (TextView) findViewById(R.id.fragment_exercise_measure_textview_subtitle);
        mTextResult = (TextView) findViewById(R.id.result);
        mImageComposeTweet = (ImageView) findViewById(R.id.compose_tweet);

        mTextTitle.setText("Stand on One Foot");


        CircleProgressBar= (CircleProgressBar) findViewById(R.id.custom_progressBar);

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the measurement routine
                if (isMeasuring == false) {
                    isMeasuring = true;
                    startMeasuring();
                } else {
                    // Do nothing
                }
            }
        });

        findViewById(R.id.compose_tweet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // At the end of a session
                if(isTweeting == true) {
//                    TweetComposer.Builder builder = new TweetComposer.Builder()
//                            .text("Your score is " + roundedAbsement + ". " + msg + "#metabalance");
//                    builder.show();

                    Intent intent = new TweetComposer.Builder(getApplicationContext())
                            .text("Your score is " + roundedAbsement + ". " + msg + " #metabalance")
                            .createIntent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }
        });

        // Init hardware
        mAccelerometer = AccelerometerManager.get(this);
        mAccelerometer.registerListenerAndConnect(this);


        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Init SoundPool
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundPoolHelper = new SoundPoolHelper(this);

    }


    @Override
    public void onResume() {
        super.onResume();

        // Keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    public void onPause() {
        super.onPause();

        // Allow screen to turn off
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Cancel the sensor and countdown activity
        mAccelerometer.stop();
//        mMeasureThread.interrupt();
        if(mTimer != null) mTimer.cancel();

        // If it's measuring and paused, go back to instruction fragment
        if(mState != STATE_FINISH) {

            mSoundPoolHelper.pause();
//            mSamples.clear();
//            backToInstructionFragment();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        mDatabaseHelper.close();
        mSoundPoolHelper.release();
        mAccelerometer.unregisterListenerAndDisconnect(this);
    }
    /**
     * Subroutine to initiate countdown and measurement functionality in a worker
     * thread. Emulates a simple State Machine to handle recurring, sequential code.
     */
    public void startMeasuring() {

        // Set the initial state
        mState = STATE_INIT;

        // reset variables
        lastUpdate = 0;
        absement = 0;
        last_x = 0;
        last_y = 0;
        last_z = 0;
        CircleProgressBar.setProgress(0);
        mTextResult.setText("");
        isTweeting = false;
        mImageComposeTweet.setVisibility(View.INVISIBLE);

        // Create a worker thread and delegate the runnable to it
        mMeasureThread = new Thread(new RunnableExerciseMeasure());
        mMeasureThread.start();
        mAccelerometer.start();
    }


    @Override
    public void onAccelerometerEvent(final float[] values) {

        if (mState == STATE_MEASURE && mTimer != null) {

            // Collect data
            float elapsedTime = mTimer.getElapsedSec();
            float x = values[0];
            float y = values[1];
            float z = values[2];

            Log.d("Debug::Elapsed Time", " " + elapsedTime);
            Log.d("Debug", x + " " + y + " " + z + " ");

            CircleProgressBar.setProgress(elapsedTime*5);
            long curTime = System.currentTimeMillis();
            if (lastUpdate != 0) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                Log.d("Debug::Diff Time", " " + diffTime);

                float diffDist = Math.abs(x - last_x) + Math.abs(y - last_y) + Math.abs(z - last_z);
                Log.d("Debug::Diff Distance", " " + diffDist);

                absement +=  diffDist * diffTime;

                last_x = x;
                last_y = y;
                last_z = z;

                roundedAbsement = (double)Math.round(absement*100)/100;

                balanceTextView.setText(" " + roundedAbsement);
                Log.d("Debug::Absement", " " + absement);
            } else {
                lastUpdate = curTime;
            }



        }
    }

    @Override
    public void onAccelerometerConnected(final boolean isBluetoothDevice, String MAC_Address) {

        // Register the device listener. The accelerometer device is guaranteed to exist and be valid
        mAccelerometer.start();
    }

    @Override
    public void onAccelerometerDisconnected() {
    }

    /** Runnable to manage collection of accelerometer data. Handles the countdown- and
     * measurement-timers. Also updates layout elements from the UI thread as appropriate */
    private class RunnableExerciseMeasure implements Runnable {

        @Override
        public void run() {

            // While the 'Finished' state hasn't been reached
            while(mState != STATE_FINISH) {

                // If the thread has been interrupted, stop executing
                if(Thread.interrupted()) {
                    return;
                }

                // Currently in the 'Initialization' state
                if(mState == STATE_INIT) {

                    // Invalidate the timer and update the state
                    mTimer = null;
                    mState = STATE_COUNTDOWN;

                    // Enqueue layout-element updates to the main (UI) thread
                    mHandler.post(mRunnableInitUI);

                    // Currently in the	'Countdown' state and the countdown timer has completed
                } else if(mState == STATE_COUNTDOWN && (mTimer != null && mTimer.isTimerDone())) {

					/* Sanity check: Ensure that the accelerometer device has been connected by
					 * the end of the countdown. If so, proceed to the measurement state. If not,
					 * skip to the final state */


                    // Invalidate the timer and update the state
                    mTimer = null;
                    mState = STATE_MEASURE;

                    // Enqueue layout-element updates to the main (UI) thread
                    mHandler.post(mRunnableCountdownUI);


                    // Currently in the 'Measuring' state and the measurement timer has completed
                } else if(mState == STATE_MEASURE && (mTimer != null && mTimer.isTimerDone())) {

                    // Update the state
                    mState = STATE_FINISH;
                }
            }

            // Currently in the 'Finished' state and the accelerometer is still connected
            if(mState == STATE_FINISH) {

                // Enqueue UI tasks to the main thread
                mHandler.post(mRunnableFinishSuccessUI);

                // Else, the measurement failed (accelerometer was disconnected)
            } else {

                // Perform the error-handling in the main thread
                mHandler.post(mRunnableFinishFailedUI);
            }
        }

        /** Runnable to enqueue UI operations in the 'Initialization' state. */
        private Runnable mRunnableInitUI = new Runnable() {

            @Override
            public void run() {

                // Update the timer-related TextViews
                mTextTitle.setText(R.string.exercise_countdown_title);
                mTextSubtitle.setText(R.string.exercise_countdown_subtitle);

				/* Start the countdown timer. Note: Android requires that
				 * CountDownTimer processes be run on the main thread. */
                mTimer = new MyCounter(10000, 100, true);
                mTimer.start();
            }
        };

        /** Runnable to enqueue UI operations in the 'Countdown' state. */
        private Runnable mRunnableCountdownUI = new Runnable() {

            @Override
            public void run() {

                // Update the timer-related TextViews
                mTextTitle.setText(R.string.exercise_messure_title);
                mTextSubtitle.setText("");

                // Start the measurement timer
                mTimer = new MyCounter(20000, 100, true);
                mTimer.start();
            }
        };

        /** Runnable to enqueue UI operations in the 'Finished' state,
         * where the measurement completed successfully. */
        private Runnable mRunnableFinishSuccessUI = new Runnable() {

            @Override
            public void run() {

                // Vibrate to indicate completion of the measurement
                vibrate(1000);
                mTextTime.setText(0.0 + " seconds");
                CircleProgressBar.setProgress(100);
                isMeasuring = false;
                mDatabaseHelper.storeBalanceNumber(absement);
                msg = scoreToRankLookUp(absement);
                mTextResult.setText(msg);
                mTextTitle.setText("");
                mImageComposeTweet.setVisibility(View.VISIBLE);
                isTweeting = true;
            }
        };

        /** Runnable to enqueue UI operations in the 'Finished' state,
         * where the measurement operation was unsuccessful. */
        private Runnable mRunnableFinishFailedUI = new Runnable() {

            @Override
            public void run() {

                // Create a toast with the appropriate error message
                Toast.makeText(getApplicationContext(), "Measurement failed. " +
                        "Accelerometer device disconnected!", Toast.LENGTH_SHORT)
                        .show();

                // Invalidate the collected samples and release the sensors

                mSoundPoolHelper.pause();
                if(mTimer != null) mTimer.cancel();



                // Return to the instruction fragment
            }
        };
    }

    /**
     * Causes the vibrator to vibrate for millis amount of milliseconds.
     * - checks Preferences first to see if vibrate is enabled
     */
    private void vibrate(int millis) {
        if(PrefUtils.getVibrateCheckBox(this)) {
            if(mVibrator.hasVibrator()) {
                mVibrator.vibrate(millis);
            }
        }
    }


    /**
     * This class is a CountDownTimer that counts with descending times.
     * It has additional functionality:
     * - can play sounds by starting a MyBeepCounter object in the last 5 seconds
     * 		(enabled by beepEnabled in constructor)
     * - updates the m_tv_time textView with the remaining time rounded to one decimal place
     * - updates the isTimerDone boolean flag when the timer finishes
     * - computes elapsed time (increasing) and returns it using getElapsedSec()
     * @author Vivian
     *
     */
    public class MyCounter extends CountDownTimer {

        private final long millisInFuture;
        private long elapsedMilliSec;	// elapsed<Time> means counting up, from 0-max
        private double sec;
        private double roundedSec;
        private boolean beepEnabled;
        private boolean beepCounterStarted;
        private boolean isTimerDone;
        private boolean isRunning;

        public MyCounter(long millisInFuture, long countDownInterval, boolean beepEnabled) {
            super(millisInFuture, countDownInterval);
            elapsedMilliSec = 0;
            this.millisInFuture = millisInFuture;
            isTimerDone = false;
            beepCounterStarted = false;
            this.beepEnabled = beepEnabled;
            isTimerDone = false;
            isRunning = false;
        }

        @Override
        public void onFinish() {
            isTimerDone = true;
            isRunning = false;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            isRunning = true;
            elapsedMilliSec = millisInFuture - millisUntilFinished;
            sec = (millisUntilFinished/1000.000);
            roundedSec = (double)Math.round(sec*10)/10;
            mTextTime.setText((roundedSec) + " seconds");

            if(millisUntilFinished < 5100 && beepEnabled && !beepCounterStarted) {
                new MyBeepCounter(5100, 1000).start();
                beepCounterStarted = true;
            }
        }

        public float getElapsedSec() {
            return (float) (elapsedMilliSec/1000.000);
        }

        public boolean isTimerDone() {
            return isTimerDone;
        }

        public boolean isTimerRunning(){
            return isRunning;
        }
    }

    /**
     * This class is a CountDownTimer that plays ping(shortDing)
     * at every tick, and plays ping(longPingDing) when it finishes.
     * @author Vivian
     *
     */
    public final class MyBeepCounter extends CountDownTimer {

        public MyBeepCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mSoundPoolHelper.shortDing();
        }

        @Override
        public void onFinish() {
            mSoundPoolHelper.longPingDing();
        }
    }

    public String scoreToRankLookUp(float balanceNum) {

        if (balanceNum <= 3000) {
            return "You have impossibly great balance, good job!";
        } else if (3000 < balanceNum && balanceNum <= 8000) {
            return "Great balance, get even better";
        } else if (8000 < balanceNum && balanceNum <= 15000) {
            return "Can you do better? Keep it up!";
        } else if (15000 < balanceNum && balanceNum <= 25000) {
            return "Good try!";
        } else {
            return "Good start";
        }
    }

}
