package ningmao.utoronto.edu.metabalance;

import android.app.Activity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import Helpers.DatabaseHelper;

/**
 * Created by ning on 2016-01-23.
 */
public class ActivityProgress extends Activity {
    private static final String TAG = ActivityProgress.class.getSimpleName();
    private DatabaseHelper mDatabaseHelper = null;
    private LineChart progress_chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chart);

        // Create database with static data for now
        mDatabaseHelper = new DatabaseHelper(this);

        progress_chart = (LineChart) findViewById(R.id.progress_chart);

        ArrayList<String> labels = new ArrayList<String>();

        ArrayList<Entry> entries = mDatabaseHelper.getProgessDataEntry(labels);

        LineDataSet dataset = new LineDataSet(entries, "Balance Value");


        LineData data = new LineData(labels, dataset);
        progress_chart.setData(data);

        progress_chart.setDescription("Your measured balance values over time");
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        progress_chart.animateY(5000);

    }
}
