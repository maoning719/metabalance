package ningmao.utoronto.edu.metabalance;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import Helpers.FragmentHelper;

/**
 * Created by ning on 2016-01-24.
 */
public class ActivityTutorial extends FragmentActivity{
    private static final String TAG = ActivityTutorial.class.getSimpleName();

    static SectionsPagerAdapter mSectionsPagerAdapter;
    static ViewPager mViewPager;
    private int NUM_FRAGMENTS = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        // Create the adapter that will return a fragment tutorial pages
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.activity_tutorial_pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Fragment[] fragList;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragList = new Fragment[NUM_FRAGMENTS];
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position < NUM_FRAGMENTS) {

                // the first fragment is always the start fragment
                fragment = FragmentTutorialStep.newInstance(position);
            } else {
                finish();
            }
            // save a reference to the requested fragment
            fragList[position] = fragment;

            // return the created fragment
            return fragment;
        }

        public Fragment getFragment(int position) {
            return fragList[position];
        }

        @Override
        public int getCount() {

            return NUM_FRAGMENTS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }



}
