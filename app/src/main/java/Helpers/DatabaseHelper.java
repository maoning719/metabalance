package Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

/**
 * Created by ning on 2016-01-23.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "metaBalance";
    public static final int DATABASE_VERSION = 1;

    private static final String TABLE_PROGRESS = "progress";
    private static final String TABLE_PROFILE = "profile";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);

        // enable foreign key support for the current database session
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE progress ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "balance REAL NOT NULL,"
                + "created_at DATETIME DEFAULT CURRENT_TIMESTAMP"
                + ");");
    }

    public void storeBalanceNumber(float balanceNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("balance", balanceNumber);
        db.insert(TABLE_PROGRESS, null, cv);

        db.close();
    }

    public ArrayList<Entry> getProgessDataEntry(ArrayList<String> labels) {
        ArrayList<Entry> entries = new ArrayList<Entry>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
                "SELECT _id, balance, created_at FROM progress;", null);

        if (cursor != null) {
            //Iterates through  rows available to cursor
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                float balance = cursor.getFloat(1);
                Log.d("balance::", " " + balance);
                Log.d("id::", " " + id);
                String time = cursor.getString(2);
                labels.add(time);

                //Creates new Exercise and adds onto list of Exercises
                entries.add(new Entry(balance, id));
            }

            cursor.close();
        }

        db.close();
        return entries;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: implement this in the future
    }
}