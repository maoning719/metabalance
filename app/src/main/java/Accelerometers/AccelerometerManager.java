package Accelerometers;

import android.content.Context;

import Accelerometers.BuiltIn.BuiltInAccelerometer;


@SuppressWarnings("unused")
/** Static class to manage available accelerometer devices */
public class AccelerometerManager {

	
	// Fully static class, user should not be able to instantiate
	private AccelerometerManager() {}

	/** 
	 * Returns an instance of the Accelerometer class (any of its children) based on 
	 * the parametrized device ID. Note: Connections or configuration of call-backs should
	 * not be done here. These should be explicitly handled by the calling method on the
	 * returned Accelerometer object.
	 * 
	 * @param context: The activity context
	 */
	public static Accelerometer get(Context context) {
		
		return new BuiltInAccelerometer(context);

	}

}
