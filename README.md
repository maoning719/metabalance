## **MetaBalance** 
It is an Android app written by Annie Mao, Veit Haensch, Jackie Ke, and Lin Han to measure and improve the balance of elderly people. It was originally written for UofTHacks 2016.

![Screenshot_2016-01-24-07-07-42.png](https://bitbucket.org/repo/M48jz4/images/2854670476-Screenshot_2016-01-24-07-07-42.png)



## Features
* Use Android phone's build-in accelerometer to measure balance
* Keep track of your balance exercise progress
* Use twitter login, tweet about your balance result and tell you who else in the community are using this app by searching #metabalance
* Simple & Intuitive Interface

## Future Improvements
* Expand from one leg standing exercise to a series of exercises under physiotherapists' supervision
* Predict the risk of falling and potentially other movement related neurological diseases using balance measure and machine learning
* Add external sensors as an anklet with good battery life and bluetooth connectivity (a "fitbit" for your feet) to track your balance everyday and everywhere


## Resources:
The Android app UI utilizes the android-circlebutton library developed by Markus Hi, which can be found at https://github.com/markushi/android-circlebutton, and the CircularProgressBar library developed by Pedramrn, which can be found at https://github.com/Pedramrn/CircularProgressBar. The app also utilizes Fabric, the Twitter mobile development platform, and the library WilliamChart written by Diogo Bernardino, the documentation for which can be found at https://docs.fabric.io/ and https://github.com/diogobernardino/WilliamChart respectively.
Targeted to SDK 21: Android version 5.0
Minimum: SDK 18: Android version 4.3